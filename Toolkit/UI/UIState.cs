﻿using Toolkit.Enums;
using UnityEngine;

namespace Toolkit.UI
{
    [CreateAssetMenu(menuName = "UI/State")]
    public class UIState : EnumMember { }
}