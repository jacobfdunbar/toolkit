﻿namespace Toolkit.UI
{
    public interface IUIStateListener
    {
        void OnStateChanged(UIState state);
    }
}