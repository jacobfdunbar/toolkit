using UnityEngine;
using UnityEngine.UI;

namespace Toolkit.UI
{
    [RequireComponent(typeof(Image))]
    public class ButtonRequireMinimumAlphaThreshold : MonoBehaviour
    {
        [SerializeField, Range(0f, 1f)] private float threshold = 0.1f;
        
        private void Awake()
        {
            var image = GetComponent<Image>();
            image.alphaHitTestMinimumThreshold = threshold;
        }
    }
}