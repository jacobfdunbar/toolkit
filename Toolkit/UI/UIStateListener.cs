﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Toolkit.UI
{
    public class UIStateListener : MonoBehaviour, IUIStateListener
    {
        [SerializeField] private UIStateManager manager = null;
        [SerializeField] private List<UIState> activeStates = null;
        [SerializeField] private UIStateEvent response = null;
        [SerializeField] private UnityEvent activeResponse = null;
        [SerializeField] private UnityEvent inactiveResponse = null;

        private void OnEnable()
        {
            if (!manager) return;
            manager.RegisterListener(this);
        }

        private void OnDisable()
        {
            if (!manager) return;
            manager.UnregisterListener(this);
        }

        public void OnStateChanged(UIState state)
        {
            response.Invoke(state);

            if (activeStates.Contains(state))
            {
                activeResponse?.Invoke();
            }
            else
            {
                inactiveResponse?.Invoke();
            }
        }

        [System.Serializable]
        public class UIStateEvent : UnityEvent<UIState> { }
    }
}