﻿using System.Collections.Generic;
using JetBrains.Annotations;
using Toolkit.Variables;
using UnityEngine;

namespace Toolkit.UI
{
    [CreateAssetMenu(menuName = "UI/State Manager")]
    public class UIStateManager : DataObject
    {
        [SerializeField] private List<UIState> states = null;
        [SerializeField] private UIState defaultState = null;
        
        public List<UIState> States => states;
        public UIState DefaultState => defaultState;
        public UIState CurrentState { get; protected set; }
        public UIState PreviousState { get; protected set; }

        private readonly List<IUIStateListener> _listeners = new List<IUIStateListener>();

        protected override void OnBegin()
        {
            base.OnBegin();
            CurrentState = DefaultState;
            OnStateChanged();
        }

        public void SetState(UIState state)
        {
            if (!states.Contains(state) || state == CurrentState) return;

            PreviousState = CurrentState;
            CurrentState = state;
            
            OnStateChanged();
        }

        public void RegisterListener(IUIStateListener listener)
        {
            if (listener == null) return;
            _listeners.Add(listener);
        }

        public void UnregisterListener(IUIStateListener listener)
        {
            _listeners.Remove(listener);
        }

        private void OnStateChanged()
        {
            for (var i = _listeners.Count - 1; i >= 0; i--)
            {
                _listeners[i].OnStateChanged(CurrentState);
            }
        }
    }
}