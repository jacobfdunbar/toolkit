using UnityEngine;

namespace Toolkit.Movement
{
    public class RandomStartingRotation : MonoBehaviour
    {
        [SerializeField] private Vector3 maxRandomRotation = default;

        private void Awake()
        {
            transform.localRotation = Quaternion.Euler(
                maxRandomRotation.x.IsZero() ? transform.localRotation.x : Random.value * maxRandomRotation.x,
                maxRandomRotation.y.IsZero() ? transform.localRotation.y : Random.value * maxRandomRotation.y,
                maxRandomRotation.z.IsZero() ? transform.localRotation.z : Random.value * maxRandomRotation.z
            );
        }
    }
}