﻿using UnityEditor;

namespace Toolkit.Variables.Editor
{
    [CustomPropertyDrawer(typeof(Vector3Reference))]
    public class Vector3ReferenceDrawer : ReferenceDrawer { }
}