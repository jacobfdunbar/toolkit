﻿using UnityEditor;

namespace Toolkit.Variables.Editor
{
    [CustomPropertyDrawer(typeof(FloatReference))]
    public class FloatReferenceDrawer : ReferenceDrawer { }
}