﻿using UnityEditor;

namespace Toolkit.Variables.Editor
{
    [CustomPropertyDrawer(typeof(RangedIntReference))]
    public class RangedIntReferenceDrawer : ReferenceDrawer { }
}