﻿using UnityEditor;

namespace Toolkit.Variables.Editor
{
    [CustomPropertyDrawer(typeof(BoolReference))]
    public class BoolReferenceDrawer : ReferenceDrawer { }
}