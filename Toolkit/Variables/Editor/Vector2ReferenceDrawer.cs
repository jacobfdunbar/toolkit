﻿using UnityEditor;

namespace Toolkit.Variables.Editor
{
    [CustomPropertyDrawer(typeof(Vector2Reference))]
    public class Vector2ReferenceDrawer : ReferenceDrawer { }
}