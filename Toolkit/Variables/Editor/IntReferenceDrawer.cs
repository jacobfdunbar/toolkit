﻿using UnityEditor;

namespace Toolkit.Variables.Editor
{
    [CustomPropertyDrawer(typeof(IntReference))]
    public class IntReferenceDrawer : ReferenceDrawer { }
}