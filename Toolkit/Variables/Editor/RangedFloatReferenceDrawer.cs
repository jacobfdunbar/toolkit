﻿using UnityEditor;

namespace Toolkit.Variables.Editor
{
    [CustomPropertyDrawer(typeof(RangedFloatReference))]
    public class RangedFloatReferenceDrawer : ReferenceDrawer { }
}