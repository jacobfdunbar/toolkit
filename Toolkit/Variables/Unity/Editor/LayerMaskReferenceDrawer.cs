﻿using UnityEditor;
using Toolkit.Variables.Editor;

namespace Toolkit.Variables.Unity.Editor
{
    [CustomPropertyDrawer(typeof(LayerMaskReference))]
    public class LayerMaskReferenceDrawer : ReferenceDrawer { }
}