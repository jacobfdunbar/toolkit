﻿using System.Collections.Generic;

namespace Toolkit.Variables.Lists
{
    /// <summary>
    ///     Base class for generic variable lists
    /// </summary>
    public abstract class ListVariable<T> : DataObject
    {
        public List<T> list = new List<T>();
    }
}