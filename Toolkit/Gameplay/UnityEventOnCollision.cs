﻿using UnityEngine;
using UnityEngine.Events;

namespace Toolkit.Gameplay.Collisions
{
    public class UnityEventOnCollision : Collidable
    {
        [SerializeField] private UnityEvent enterEvent = default;
        [SerializeField] private UnityEvent stayEvent = default;
        [SerializeField] private UnityEvent exitEvent = default;

        protected override void OnEnter(Collision collision) => enterEvent?.Invoke();
        protected override void OnStay(Collision collision) => stayEvent?.Invoke();
        protected override void OnExit(Collision collision) => exitEvent?.Invoke();
    }
}