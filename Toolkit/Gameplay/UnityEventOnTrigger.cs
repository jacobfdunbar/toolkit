﻿using UnityEngine;
using UnityEngine.Events;

namespace Toolkit.Gameplay.Collisions
{
    public class UnityEventOnTrigger : Triggerable
    {
        [SerializeField] private UnityEvent enterEvent = default;
        [SerializeField] private UnityEvent stayEvent = default;
        [SerializeField] private UnityEvent exitEvent = default;

        protected override void OnEnter(Collider other) => enterEvent?.Invoke();
        protected override void OnStay(Collider other) => stayEvent?.Invoke();
        protected override void OnExit(Collider other) => exitEvent?.Invoke();
    }
}