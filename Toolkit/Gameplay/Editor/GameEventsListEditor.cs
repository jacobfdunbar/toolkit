﻿using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace Toolkit.Gameplay.Collisions
{
    public class GameEventsListEditor : Editor
    {
        private ReorderableList _events;

        private void OnEnable()
        {
            _events = new ReorderableList(
                serializedObject,
                serializedObject.FindProperty("gameEvents"),
                true,
                true,
                true,
                true
            )
            {
                drawHeaderCallback = rect =>
                {
                    EditorGUI.LabelField(new Rect(rect.x, rect.y, 200, EditorGUIUtility.singleLineHeight),
                        "Events");
                },
                drawElementCallback = (rect, index, active, focused) =>
                {
                    var element = _events.serializedProperty.GetArrayElementAtIndex(index);
                    rect.y += 2;
                    EditorGUI.PropertyField(
                        new Rect(rect.x, rect.y, 200, EditorGUIUtility.singleLineHeight),
                        element,
                        GUIContent.none
                    );
                }
            };
        }
        
        public override void OnInspectorGUI()
        {
            _events.DoLayoutList();
            serializedObject.ApplyModifiedProperties();
        }
    }
    
    [CustomEditor(typeof(GameEventOnTrigger))]
    public class GameEventOnTriggerEditor : GameEventsListEditor { }
    
    [CustomEditor(typeof(GameEventOnCollision))]
    public class GameEventOnCollisionEditor : GameEventsListEditor { }
}