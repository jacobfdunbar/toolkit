﻿using Toolkit.Utilities;
using UnityEngine;

namespace Toolkit.Gameplay.Collisions
{
    [RequireComponent(typeof(Collider))]
    public abstract class Triggerable : MonoBehaviour
    {
        [SerializeField] private LayerMask triggerMask = default;

        private void OnTriggerEnter(Collider other)
        {
            if (!triggerMask.ContainsLayer(other.gameObject.layer)) return;
            OnEnter(other);
        }

        private void OnTriggerStay(Collider other)
        {
            if (!triggerMask.ContainsLayer(other.gameObject.layer)) return;
            OnStay(other);
        }

        private void OnTriggerExit(Collider other)
        {
            if (!triggerMask.ContainsLayer(other.gameObject.layer)) return;
            OnExit(other);
        }

        protected virtual void OnEnter(Collider other) { }

        protected virtual void OnStay(Collider other) { }

        protected virtual void OnExit(Collider other) { }
    }
}