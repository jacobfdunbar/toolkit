﻿using UnityEngine;

namespace Toolkit.Gameplay.Collisions.DebugUtilities
{
    public class DebugLogOnCollision : Collidable
    {
        protected override void OnEnter(Collision collision)
        {
            Debug.Log($"{name} hit by {collision.collider.name}.");
        }
    }
}