﻿using UnityEngine;

namespace Toolkit.Gameplay.Collisions.DebugUtilities
{
    public class DebugLogOnTrigger : Triggerable
    {
        protected override void OnEnter(Collider other)
        {
            Debug.Log($"{name} triggered by {other.name}.");
        }
    }
}