﻿using System.Collections.Generic;
using System.Linq;
using Toolkit.Events;
using UnityEngine;

namespace Toolkit.Gameplay.Collisions
{
    public class GameEventOnCollision : Collidable
    {
        [SerializeField] private List<GameEvent> gameEvents = new List<GameEvent>();

        protected override void OnEnter(Collision collision)
        {
            foreach (var gameEvent in gameEvents.Where(gameEvent => gameEvent != null))
            {
                gameEvent.Raise();
            }
        }
    }
}