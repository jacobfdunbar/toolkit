﻿using Toolkit.Utilities;
using UnityEngine;

namespace Toolkit.Gameplay.Collisions
{
    [RequireComponent(typeof(Collider))]
    public abstract class Collidable : MonoBehaviour
    {
        [SerializeField] private LayerMask collisionMask = default;

        private void OnCollisionEnter(Collision collision)
        {
            if (!collisionMask.ContainsLayer(collision.gameObject.layer)) return;
            OnEnter(collision);
        }

        private void OnCollisionStay(Collision collision)
        {
            if (!collisionMask.ContainsLayer(collision.gameObject.layer)) return;
            OnStay(collision);
        }

        private void OnCollisionExit(Collision collision)
        {
            if (!collisionMask.ContainsLayer(collision.gameObject.layer)) return;
            OnExit(collision);
        }

        protected virtual void OnEnter(Collision collision) { }

        protected virtual void OnStay(Collision collision) { }

        protected virtual void OnExit(Collision collision) { }
    }
}