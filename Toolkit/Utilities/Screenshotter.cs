﻿using Sirenix.OdinInspector;
using UnityEngine;

public class Screenshotter : MonoBehaviour
{
    [SerializeField] private string filePath = null;

    [Button("Take Screenshot")]
    private void Screenshot()
    {
        Debug.Log("Screenshot taken!");
        ScreenCapture.CaptureScreenshot($"{filePath}{System.DateTime.UtcNow.ToFileTimeUtc().ToString()}.png", 4);
    }
}