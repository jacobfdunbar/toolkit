﻿using System.Collections.Generic;
using TMPro;
using Toolkit.Utilities.DeveloperConsole.Commands;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace Toolkit.Utilities.DeveloperConsole
{
    public class DeveloperConsoleBehaviour : MonoBehaviour
    {
        [SerializeField] private string prefix = string.Empty;
        [SerializeField] private ConsoleCommand[] commands = new ConsoleCommand[0];
        
        [Header("Console Key Bindings")]
        [SerializeField] private InputAction consoleToggleAction = default;
        [SerializeField] private InputAction previousInputAction = default;
        [SerializeField] private InputAction nextInputAction = default;
        
        [Header("UI")]
        [SerializeField] private GameObject canvas = null;
        [SerializeField] private TMP_InputField inputField = null;
        [SerializeField] private ScrollRect scrollRect = null;
        [SerializeField] private TextMeshProUGUI logText = null;

        private float _previousTimeScale;

        private static DeveloperConsoleBehaviour _instance;

        private DeveloperConsole _developerConsole;
        
        private readonly List<string> _inputHistory = new List<string>();
        private int _historyIndex = -1;

        private void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(this);
                return;
            }

            _instance = this;
            _developerConsole = new DeveloperConsole(prefix, commands);
            consoleToggleAction.performed += Toggle;
            previousInputAction.performed += PreviousInput;
            nextInputAction.performed += NextInput;
            DontDestroyOnLoad(gameObject);
        }

        private void OnEnable()
        {
            consoleToggleAction.Enable();
        }

        private void OnDisable()
        {
            consoleToggleAction.Disable();
        }

        public void Toggle(InputAction.CallbackContext context)
        {
            if (!context.action.triggered) return;

            if (canvas.activeSelf)
            {
                Time.timeScale = _previousTimeScale;
                canvas.SetActive(false);
                
                previousInputAction.Disable();
                nextInputAction.Disable();
            }
            else
            {
                _previousTimeScale = Time.timeScale;
                Time.timeScale = 0;
                canvas.SetActive(true);
                inputField.ActivateInputField();
                
                previousInputAction.Enable();
                nextInputAction.Enable();
            }
        }

        public void PreviousInput(InputAction.CallbackContext context)
        {
            if (!context.action.triggered || !canvas.activeSelf) return;
            _historyIndex = Mathf.Clamp(_historyIndex + 1, -1, _inputHistory.Count - 1);
            if (_historyIndex < 0) return;
            inputField.text = _inputHistory[_historyIndex];
        }

        public void NextInput(InputAction.CallbackContext context)
        {
            if (!context.action.triggered || !canvas.activeSelf) return;
            _historyIndex = Mathf.Clamp(_historyIndex - 1, -1, _inputHistory.Count - 1);
            inputField.text = _historyIndex < 0 ? "" : _inputHistory[_historyIndex];
        }

        public void ProcessCommand(string input)
        {
            if (!canvas.activeSelf || input == null || input.Length == 0) return;
            _inputHistory.Insert(0, input);
            _instance._historyIndex = -1;
            var (success, message) = _developerConsole.ProcessCommand(input);
            inputField.text = string.Empty;
            inputField.ActivateInputField();
            if (!success) Log(input);
            Log(message);
            scrollRect.normalizedPosition = Vector2.zero;
        }

        public static void Log(string message, Color color = default)
        {
            if (_instance == null || message == null || _instance.logText == null) return;
            color = color == default ? Color.white : color;
            _instance.logText.text += $"<color=#{ColorUtility.ToHtmlStringRGB(color)}>\n> {message}</color>";
        }

        public static void Clear()
        {
            if (_instance == null || _instance.logText == null) return;
            _instance.logText.text = "\n";
            _instance._inputHistory.Clear();
            _instance._historyIndex = -1;
        }
    }
}