﻿using System.Collections.Generic;
using System.Linq;
using Toolkit.Utilities.DeveloperConsole.Commands;

namespace Toolkit.Utilities.DeveloperConsole 
{
    public class DeveloperConsole
    {
        private readonly string _prefix;
        private readonly Dictionary<string, IConsoleCommand> _commands = new Dictionary<string, IConsoleCommand>();
        
        public DeveloperConsole(string prefix, IEnumerable<IConsoleCommand> commands)
        {
            _prefix = prefix;
            foreach (var command in commands)
            {
                _commands.Add(command.CommandWord.ToLower(), command);
            }
        }

        public (bool, string) ProcessCommand(string commandInput, string[] args)
        {
            if (!_commands.TryGetValue(commandInput.ToLower(), out var command)) return (false, "<color=#FF0000>Invalid command.</color>");
            var (success,  message) = command.Process(args);
            var colorFormat = success ? "<color=#00FF00>" : "<color=#FF0000>";
            return (success, $"{colorFormat}{message}</color>");
        }

        public (bool, string) ProcessCommand(string input)
        {
            if (string.IsNullOrEmpty(input)) return (false, null);
            if (_prefix != null && !input.StartsWith(_prefix)) return (false, null);
            
            if (_prefix != null)
            {
                input = input.Remove(0, _prefix.Length);
            }

            var splitInput = input.Split(' ');
            var command = splitInput[0];
            var args = splitInput.Skip(1).ToArray();
            return ProcessCommand(command, args);
        }
    }
}