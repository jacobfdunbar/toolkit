﻿namespace Toolkit.Utilities.DeveloperConsole.Commands
{
    public interface IConsoleCommand
    {
        string CommandWord { get; }
        (bool, string) Process(string[] args);
    }
}