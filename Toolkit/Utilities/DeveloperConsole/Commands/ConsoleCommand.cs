﻿using Toolkit.Variables;
using UnityEngine;

namespace Toolkit.Utilities.DeveloperConsole.Commands {
    public abstract class ConsoleCommand : DataObject, IConsoleCommand
    {
        [SerializeField] private string commandWord = string.Empty;
        
        public string CommandWord => commandWord;

        public abstract (bool, string) Process(string[] args);
    }
}