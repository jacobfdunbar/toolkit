﻿using UnityEngine;

namespace Toolkit.Utilities.DeveloperConsole.Commands
{
    [CreateAssetMenu(menuName = "Utilities/Developer Console/Commands/Log Command")]
    public class LogCommand : ConsoleCommand
    {
        public override (bool, string) Process(string[] args)
        {
            if (args == null || args.Length == 0) return (false, "Invalid arguments.");
            var logText = string.Join(" ", args);
            Debug.Log(logText);
            return (true, $"Logged: \"{logText}\"");
        }
    }
}