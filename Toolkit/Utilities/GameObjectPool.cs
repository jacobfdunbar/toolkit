﻿using System.Collections.Generic;
using UnityEngine;

namespace Toolkit.Utilities
{
    public class GameObjectPool<T> where T : Component
    {
        private const int MAX_POOL_SIZE = 200;

        private readonly Queue<T> _pool = new Queue<T>();
        private readonly GameObject _prefab;
        private readonly Transform _parent;

        public GameObjectPool(GameObject prefab, Transform parent)
        {
            _prefab = prefab;
            _parent = parent;
        }

        public T Get(Transform parent = null)
        {
            while (true)
            {
                if (_pool.Count == 0)
                {
                    var newObj = Object.Instantiate(_prefab, parent).GetComponent<T>();
                    newObj.gameObject.SetActive(true);
                    return newObj;
                }

                var item = _pool.Dequeue();
                if (item == null) continue;

                item.gameObject.SetActive(true);
                item.transform.SetParent(parent);

                return item;
            }
        }

        public void Put(T obj)
        {
            if (_pool.Count < MAX_POOL_SIZE)
            {
                obj.transform.SetParent(_parent);
                obj.gameObject.SetActive(false);
                _pool.Enqueue(obj);
            }
            else
            {
                Object.Destroy(obj.gameObject);
            }
        }
    }
}