using UnityEngine;

namespace Toolkit.Generic
{
    public class LookAtCamera : MonoBehaviour
    {
        [SerializeField] private bool invert = false;

        private void OnEnable() => Look();

        public void Look()
        {
            var cam = Camera.main;
            if (!cam) return;
            transform.rotation = Quaternion.LookRotation(invert
                ? (transform.position - cam.transform.position).normalized
                : (cam.transform.position - transform.position).normalized);
        }
    }
}