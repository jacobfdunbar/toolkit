using System;
using UnityEngine;

namespace Toolkit.Generic
{
    [Serializable]
    public struct SimpleGradient
    {
        public Color TopColor;
        public Color BottomColor;
    }
}