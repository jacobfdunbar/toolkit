﻿namespace Toolkit.Events
{
    public interface IGameEventListener
    {
        void OnEventRaised();
    }
}