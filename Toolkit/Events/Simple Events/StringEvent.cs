﻿using UnityEngine;
using UnityEngine.Events;

namespace Toolkit.Events.Simple
{
    [CreateAssetMenu(menuName = "Event/Simple/String")]
    public class StringEvent : GenericGameEvent<string, UnityStringEvent, StringEvent> {}

    [System.Serializable]
    public class UnityStringEvent : UnityEvent<string> {}
}