using UnityEngine;
using UnityEngine.Events;

namespace Toolkit.Events.Simple
{
    [CreateAssetMenu(menuName = "Event/Simple/Bool")]
    public class BoolEvent : GenericGameEvent<bool, UnityBoolEvent, BoolEvent> {}

    [System.Serializable]
    public class UnityBoolEvent : UnityEvent<bool> {}
}