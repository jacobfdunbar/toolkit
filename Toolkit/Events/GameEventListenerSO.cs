﻿using UnityEngine;
using UnityEngine.Events;

namespace Toolkit.Events
{
    [CreateAssetMenu(menuName = "Event/ListenerSO")]
    public class GameEventListenerSO : ScriptableObject, IGameEventListener
    {
        [SerializeField] private GameEvent Event = null;
        [SerializeField] private UnityEvent Response = null;

        private void OnEnable()
        {
            if (!Event) return;
            Event.RegisterListener(this);
        }

        private void OnDisable()
        {
            if (!Event) return;
            Event.UnregisterListener(this);
        }

        public void OnEventRaised()
        {
            Response.Invoke();
        }
    }
}