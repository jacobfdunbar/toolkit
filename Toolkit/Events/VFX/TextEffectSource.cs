using TMPro;
using Toolkit.Generic;
using UnityEngine;

namespace Toolkit.Events.VFX
{
    public class TextEffectSource : VFXSource
    {
        [SerializeField] private TMP_Text text = default;
        [SerializeField] private LookAtCamera lookAtCamera = default;

        public void SetText(string effectText, Color color)
        {
            text.text = effectText;
            text.enableVertexGradient = false;
            text.color = color;
            lookAtCamera.Look();
        }

        public void SetText(string effectText, Color topColor, Color bottomColor)
        {
            text.text = effectText;
            text.enableVertexGradient = true;
            text.colorGradient = new VertexGradient(topColor, topColor, bottomColor, bottomColor);
            lookAtCamera.Look();
        }
    }
}