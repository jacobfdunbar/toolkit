﻿using System.Collections.Generic;
using UnityEngine;
using Toolkit.Variables;

namespace Toolkit.Enums
{
    [CreateAssetMenu(menuName = "Enum/Enumeration")]
    public class Enumeration : DataObject
    {
        public List<EnumMember> Members;

        public EnumMember GetMemberByNameHash(int hash)
        {
            return Members.Find(member => member.Name.GetHashCode() == hash);
        }
    }
}