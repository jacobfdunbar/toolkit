﻿using Toolkit.Variables;
using UnityEngine;

namespace Toolkit.Enums
{
    [CreateAssetMenu(menuName = "Enum/Member")]
    [System.Serializable]
    public class EnumMember : DataObject
    {
        public string Name => name;
    }
}