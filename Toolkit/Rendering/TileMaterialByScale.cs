using UnityEngine;

namespace Toolkit.Rendering
{
    [ExecuteInEditMode, RequireComponent(typeof(MeshRenderer))]
    public class TileMaterialByScale : MonoBehaviour
    {
        private static readonly int mainTextSt = Shader.PropertyToID("_MainTex_ST");
        
        private MeshRenderer _meshRenderer;
        private MaterialPropertyBlock _propertyBlock;

        private void Awake()
        {
            _meshRenderer = GetComponent<MeshRenderer>();
            _propertyBlock = new MaterialPropertyBlock();
        }

        private void Update()
        {
            if (!transform.hasChanged) return;
            if (_propertyBlock == null) _propertyBlock = new MaterialPropertyBlock();
            _propertyBlock.SetVector(mainTextSt, new Vector4(1f / transform.localScale.x, transform.localScale.y, 0f, 0f));
            _meshRenderer.SetPropertyBlock(_propertyBlock);
        }
    }
}